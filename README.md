**ZOOKEEPER**

bin/zookeeper-server-start.sh config/zookeeper.properties

   **Windows:**

   cd bin/windows

   zookeeper-server-start.bat ../../config/zookeeper.properties

**SERVER**

bin/kafka-server-start.sh config/server.properties

  **Windows:**

   cd bin/windows

   kafka-server-start.bat ../../config/server.properties

**CREATE TOPIC test**

bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic test

**PRODUCER**

bin/kafka-console-producer.sh --broker-list localhost:9092 --topic test