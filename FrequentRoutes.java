package FrequentRoutes;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.streaming.Duration;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.apache.spark.streaming.api.java.JavaPairReceiverInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka.KafkaUtils;
import scala.Tuple2;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class FrequentRoutes {

    public static void main(String[] args) throws ParseException {

    	System.setProperty("hadoop.home.dir", "d:\\winutil\\");
        //Get rid off annoying log messages
        Logger.getLogger("org").setLevel(Level.OFF);
        Logger.getLogger("akka").setLevel(Level.OFF);

        streamingExampleQuery1(args);

    }

    private static void streamingExampleQuery1(String[] args){

        GridUtils grid = new GridUtils(GridUtils.SQUAREDIMQ1);// quadrados de 500x500m para a questao 1
        SparkConf sparkConf = new SparkConf().setAppName("FrequentRoutesChallengeStreammingQuery1").setMaster("local[2]");
        // Create the context with a 1 second batch size
        JavaStreamingContext jssc = new JavaStreamingContext(sparkConf, new Duration(1000));
        jssc.checkpoint("checkpoint");

        int numThreads = Integer.parseInt(args[1]);
        Map<String, Integer> topicMap = new HashMap<String, Integer>();
        String[] topics = args[0].split(",");
        for (String topic: topics) {
            topicMap.put(topic, numThreads);
        }

        JavaPairReceiverInputDStream<String, String> messages =
                KafkaUtils.createStream(jssc, "streamdimarota1.cloudapp.net:2181", "dummy-group", topicMap);

        JavaDStream<Tuple2<String, Integer>> result = messages.map(s -> new Route( Double.valueOf(s._2.split(",")[6]),Double.valueOf(s._2.split(",")[7]),
                Double.valueOf(s._2.split(",")[8]),Double.valueOf(s._2.split(",")[9]),grid))
                .filter(route -> !route.isOutlier())
                .mapToPair(route->new Tuple2<>(route.toStringRouteID(),1))
                .reduceByKeyAndWindow( (a,b) -> a+b , (a,b) -> a-b , Durations.minutes(10),Durations.seconds(1))
                .map( row -> new Tuple2<>(row._2,row._1)).transform(row ->row.sortBy(r -> r._1, false, 0)).map(row -> new Tuple2<>(row._2,row._1));

        result.print(10);

        jssc.start();
        jssc.awaitTermination();
        jssc.close();

    }


}


class Route implements Serializable {

    private static final long serialVersionUID = -2377063410444461445L;
    double pickup_longt;
    double pickup_lat;
    double dropoff_longt;
    double dropoff_lat;
    GridUtils grid;
    Date pickupTime = null;
    Date dropOffTime = null;
    double tip = 0;
    double fare = 0;
    String taxi = "N/A" ;


    public Route(double pickup_longt, double pickup_lat, double dropoff_longt, double dropoff_lat,GridUtils grid){
        this.pickup_longt = pickup_longt;
        this.pickup_lat = pickup_lat;
        this.dropoff_longt = dropoff_longt;
        this.dropoff_lat = dropoff_lat;
        this.grid = grid;
    }

    public Route(double pickup_longt, double pickup_lat, double dropoff_longt, double dropoff_lat,double tip, double fare,String taxi,String pickupTime, String dropOffTime, GridUtils grid) throws ParseException{
        this.pickup_longt = pickup_longt;
        this.pickup_lat = pickup_lat;
        this.dropoff_longt = dropoff_longt;
        this.dropoff_lat = dropoff_lat;
        this.grid = grid;
        this.tip = tip;
        this.fare = fare;
        this.taxi = taxi;

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        this.pickupTime = format.parse(pickupTime);
        this.dropOffTime = format.parse(dropOffTime);
        //System.out.println(this.pickupTime +" , "+pickupTime);
        //System.out.println(this.dropOffTime + " , "+dropOffTime);
    }

    public double getProfit(){
        return this.tip + this.fare;
    }


    //Distance of pickup point and init point - in degrees
    public String distanceDegrees(){
        return "("+grid.distanceDegrees(pickup_lat,pickup_longt) + "," + grid.distanceDegrees(dropoff_lat,dropoff_longt)+")";
    }

    //Distance of dropoff point and init point - in meters
    public String distanceMeters(){
        return "("+grid.distanceMeters(grid.distanceDegrees(pickup_lat,pickup_longt)) + "," + grid.distanceMeters(grid.distanceDegrees(dropoff_lat,dropoff_longt))+")";
    }


    public String toStringDegrees(){
        return "("+pickup_lat +","+pickup_longt+") - ("+dropoff_lat +","+dropoff_longt+")";
    }

    public String toStringPickupAreaID(){
        return grid.getCellID(pickup_lat, pickup_longt);
    }

    public String toStringDropAreaID(){
        return grid.getCellID(dropoff_lat, dropoff_longt);
    }


    public String toStringRouteID(){
        return "("+grid.getCellID(pickup_lat, pickup_longt) + "," + grid.getCellID(dropoff_lat, dropoff_longt)+")";
    }

    public boolean isOutlier(){
        return grid.isOutlier(pickup_lat, pickup_longt) || grid.isOutlier(dropoff_lat, dropoff_longt);
    }

}


class GridUtils implements Serializable{ //https://pencilplow.files.wordpress.com/2013/04/lat-long-flat-graphic.gif

    private static final long serialVersionUID = -5806871841712378278L;

    public static final double LIMIT = 150000;

    public static final double SQUAREDIMQ1 = 500; //bounding box of a point
    public static final double SQUAREDIMQ2 = 250;

    public static final double SQUAREDIMLAT = 0.004491556; //step de 500m equivale a SQUAREDIMLAT graus vertical (Square dimension)
    public static final double SQUAREDIMLONGT = 0.005986; //step de 500m equivale a SQUAREDIMLONGT graus horizontal

    public static final double INITLT = 41.474937;
    public static final double INITLG= -74.913585;

    private double dim;
    private double dimLt = SQUAREDIMLAT; // step em graus na latitude
    private double dimLg = SQUAREDIMLONGT; //step em graus na longitude

    public GridUtils(double dim){
        this.dim = dim;

        if(dim == SQUAREDIMQ2){ //questao 2
            dimLt = SQUAREDIMLAT/2;
            dimLg = SQUAREDIMLONGT/2;
        }

    }

    //Funçao q vai filtrar os registos q n sao outliers
    public boolean isOutlier(double lat, double longt){
        Distance d  = this.distanceDegrees(lat, longt);
        Distance d_meters = this.distanceMeters(d);
        boolean offlimit_x = (longt < INITLG && d_meters.d_ew > SQUAREDIMQ1) || d_meters.d_ew > LIMIT ;
        boolean offlimit_y = (lat > INITLT && d_meters.d_ns > SQUAREDIMQ1 ) || d_meters.d_ns > LIMIT; //erro aqui!!!

        return offlimit_x || offlimit_y;
    }

    //Distancia em relacao ao ponto de origem em graus latitude
    public Distance distanceDegrees(double lat, double longt){ // http://stackoverflow.com/questions/1664799/calculating-distance-between-two-points-using-pythagorean-theorem
        return new Distance( Math.abs(Math.abs(lat) - Math.abs(INITLT)) , Math.abs(Math.abs(longt) - Math.abs(INITLG)) );
    }

    public Distance distanceMeters(Distance degrees){
        double d_ew_meters = ((degrees.d_ew * dim)/dimLg);
        double d_ns_meters = ((degrees.d_ns * dim)/dimLt);
        return new Distance(d_ns_meters,d_ew_meters);
    }

    public String getCellID(double lat, double longt){
        Distance d  = this.distanceMeters(this.distanceDegrees(lat, longt));

        double d_ew_meters = d.d_ew; //Integer.parseInt(String.valueOf(d.d_ew).split("\\.")[0]);
        double d_ns_meters = d.d_ns;// Integer.parseInt(String.valueOf(d.d_ns).split("\\.")[0]);

        return getCell(d_ew_meters) +"."+ getCell(d_ns_meters);
    }

    public int getCell(double distanceMeters){
        return (int)Math.round(distanceMeters/dim)+1;
    }

    private class Distance{
        double d_ew; //east-west
        double d_ns; //north-south

        public Distance(double ns,double ew){
            this.d_ew = ew;
            this.d_ns = ns;
        }

        @SuppressWarnings("unused")
        public double straightDistance(){
            return Math.sqrt(d_ew * d_ew + d_ns * d_ns);
        }

        public String toString(){
            return "[EW:"+d_ew+"; NS:"+d_ns+"]";
        }
    }


}

class tupleComparator implements Comparator<Tuple2<String,Integer>>, java.io.Serializable{

    private static final long serialVersionUID = -8067888229300922700L;

    public int compare(Tuple2<String, Integer> o1, Tuple2<String, Integer> o2) {
        if(o1._2 > o2._2)
            return 1;
        else if(o1._2 < o2._2)
            return -1;
        return 0;
    }

}
